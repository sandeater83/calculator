package com.example.stuartadams.calculator;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class MainActivity extends ActionBarActivity {

    @InjectView(R.id.btnAdd) Button addButton;
    @InjectView(R.id.btnClear) Button clearButton;
    @InjectView(R.id.btnDelete) Button deleteButton;
    @InjectView(R.id.btnDivideFunction) Button divideButton;
    @InjectView(R.id.btnDot) Button dotButton;
    @InjectView(R.id.btnOne) Button oneButton;
    @InjectView(R.id.btnTwo) Button twoButton;
    @InjectView(R.id.btnThree) Button threeButton;
    @InjectView(R.id.btnFour) Button fourButton;
    @InjectView(R.id.btnFive) Button fiveButton;
    @InjectView(R.id.btn6) Button sixButton;
    @InjectView(R.id.btnSeven) Button sevenButton;
    @InjectView(R.id.btnEight) Button eightButton;
    @InjectView(R.id.btnNine) Button nineButton;
    @InjectView(R.id.btnZero) Button zeroButton;
    @InjectView(R.id.btnEquals) Button equalsButton;
    @InjectView(R.id.btnPercent) Button percentButton;
    @InjectView(R.id.btnSubtract) Button subtractButton;
    @InjectView(R.id.btnMultiplyFunction) Button multiplyButton;
    @InjectView(R.id.txtCalcDisplay) TextView calculatorDisplay;
    private float totalValue;
    private boolean clearDisplay = false;
    private boolean divideByZero = false;
    private String lastMathFunction = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        totalValue = 0;
    }

    @OnClick(R.id.btnClear)
            public void clearCalculatorDisplay()
    {
        calculatorDisplay.setText("0");
        clearDisplay = false;
        totalValue = 0;
        divideByZero = false;
    }

    @OnClick(R.id.btnDelete)
    public void deleteDigit()
    {
        if(!divideByZero)
        {
            if(lastMathFunction.equalsIgnoreCase("equal")
                    || calculatorDisplay.getText().toString().length() == 1)
            {
                calculatorDisplay.setText("0");
            }
            else
            {
                String temp = "";
                temp = calculatorDisplay.getText()
                        .toString()
                        .substring(0,calculatorDisplay.getText().toString().length() - 1);
                calculatorDisplay.setText(temp);
            }
        }
    }

    @OnClick({ R.id.btnOne, R.id.btnTwo, R.id.btnThree, R.id.btnFour, R.id.btnFive, R.id.btn6,
            R.id.btnSeven, R.id.btnEight, R.id.btnNine, R.id.btnZero})
    public void tapDigit(Button button)
    {
        if(!divideByZero)
        {
            if(calculatorDisplay.getText().toString().length() < 11)
            {
                if(calculatorDisplay.getText().toString().equalsIgnoreCase("0")
                        || clearDisplay)
                {
                    calculatorDisplay.setText(button.getText());
                }
                else
                {
                    calculatorDisplay.setText(calculatorDisplay.getText().toString()
                            + button.getText().toString());
                }
            }
            clearDisplay = false;
        }
    }

    @OnClick(R.id.btnDot)
    public void addDecimalPoint(Button button)
    {
        if(!divideByZero)
        {
            if(clearDisplay)
            {
                calculatorDisplay.setText("0.");
                clearDisplay = false;
            }
            if(!calculatorDisplay.getText().toString().contains("."))
            {
                calculatorDisplay.setText(calculatorDisplay.getText().toString()
                        + button.getText().toString());
            }
        }
    }

//    @OnClick(R.id.btnAdd)
//     public void addFunction()
//    {
//        if(!divideByZero)
//        {
//            clearDisplay = true;
//            if(!lastMathFunction.equalsIgnoreCase("equal"))
//            {
//                if(totalValue == 0)
//                {
//                    totalValue = Float.parseFloat(calculatorDisplay.getText().toString());
//                }
//                else
//                {
//                    totalValue += Float.parseFloat(calculatorDisplay.getText().toString());
//                }
//            }
//        }
//        lastMathFunction = "add";
//    }
//
//    @OnClick(R.id.btnSubtract)
//    public void subtractFunction()
//    {
//        if(!divideByZero)
//        {
//            clearDisplay = true;
//            if(!lastMathFunction.equalsIgnoreCase("equal"))
//            {
//                if(totalValue == 0)
//                {
//                    totalValue = Float.parseFloat(calculatorDisplay.getText().toString());
//                }
//                else
//                {
//                    totalValue -= Float.parseFloat(calculatorDisplay.getText().toString());
//                }
//
//            }
//            lastMathFunction = "subtract";
//        }
//    }
//
//    @OnClick(R.id.btnDivideFunction)
//    public void divideFunction()
//    {
//        if(!divideByZero)
//        {
//            if(!lastMathFunction.equalsIgnoreCase("equal")) {
//                clearDisplay = true;
//                if (calculatorDisplay.getText().toString().equalsIgnoreCase("0") || totalValue == 0) {
//                    calculatorDisplay.setText("Err: Div by 0");
//                    divideByZero = true;
//                } else {
//                    totalValue /= Float.parseFloat(calculatorDisplay.getText().toString());
//                }
//            }
//            lastMathFunction = "divide";
//        }
//    }
//
//    @OnClick(R.id.btnMultiplyFunction)
//    public void multiplyFunction()
//    {
//
//        if(!divideByZero)
//        {
//            if(!lastMathFunction.equalsIgnoreCase("equal")) {
//                clearDisplay = true;
//                if (totalValue == 0) {
//                    totalValue = Float.parseFloat(calculatorDisplay.getText().toString());
//                } else {
//                    totalValue *= Float.parseFloat(calculatorDisplay.getText().toString());
//                }
//            }
//            lastMathFunction = "multiply";
//        }
//    }

    @OnClick({R.id.btnEquals, R.id.btnAdd, R.id.btnSubtract,
            R.id.btnMultiplyFunction, R.id.btnDivideFunction})
    public void equalFunction(Button button)
    {
        if(!divideByZero)
        {
            if(totalValue == 0 || lastMathFunction.equalsIgnoreCase("="))
            {
                totalValue = Float.parseFloat(calculatorDisplay.getText().toString());
            }
            else
            {
                switch (lastMathFunction)
                {
                    case "+":
                        totalValue += Float.parseFloat(calculatorDisplay.getText().toString());
                        break;
                    case "-":
                        totalValue -= Float.parseFloat(calculatorDisplay.getText().toString());
                        break;
                    case "/":
                        if(calculatorDisplay.getText().toString().equalsIgnoreCase("0")
                                || totalValue == 0)
                        {
                            calculatorDisplay.setText("Err: Div by 0");
                            divideByZero = true;
                        }
                        else
                        {
                            totalValue /= Float.parseFloat(calculatorDisplay.getText().toString());
                        }
                        break;
                    case "*":
                        totalValue *= Float.parseFloat(calculatorDisplay.getText().toString());
                        break;
                }
            }

            lastMathFunction = button.getText().toString();

            if(button.getText().toString().equalsIgnoreCase("="))
            {
                calculatorDisplay.setText(""+totalValue);
            }

            clearDisplay = true;
        }
    }

    @OnClick(R.id.btnPercent)
    public void percentFunction()
    {
        float temp = 0;
        temp = Float.parseFloat(calculatorDisplay.getText().toString());
        calculatorDisplay.setText("" + temp/100);
    }
}
